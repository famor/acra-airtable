# Copyright 2020 Felipe Morato me@fmorato.com
#
# This file is under the MIT License.
# See the file `LICENSE` for details.
import logging
import os
import sys
from typing import Optional

import ujson
from aiohttp import ClientConnectorError, ClientSession
from airtable import Airtable
from pydantic import BaseModel, ValidationError
from sanic import Sanic, response
from sanic.log import LOGGING_CONFIG_DEFAULTS
from sanic.request import Request

logging.getLogger("telethon").setLevel(logging.WARNING)
logger = logging.getLogger("acra-airtable")

logging_config = LOGGING_CONFIG_DEFAULTS
logging_config["loggers"]["acra-airtable"] = {
    "level": "INFO",
    "handlers": ["acra-airtable"],
}
logging_config["handlers"]["acra-airtable"] = {
    "class": "logging.StreamHandler",
    "formatter": "acra-airtable",
    "stream": sys.stdout,
}
logging_config["formatters"]["acra-airtable"] = {
    "format": "%(asctime)s [%(process)d] [%(levelname)s] (%(name)s) "
    "[%(module)s::%(funcName)s():%(lineno)d] %(message)s",
    "datefmt": "[%Y-%m-%d %H:%M:%S]",
    "class": "logging.Formatter",
}

SESSION = os.environ.get("SESSION", "acra-airtable")

# Required
AIRTABLE_API_KEY = os.environ.get("AIRTABLE_API_KEY")
ACRA_BASE_ID = os.environ.get("ACRA_BASE_ID")

if not (AIRTABLE_API_KEY or ACRA_BASE_ID):
    logger.error("At least one of the required env vars was not set")

# Used for notification
ACRA_TABLE_ID = os.environ.get("ACRA_TABLE_ID")
TELEGRAM_NOTIFICATION_URL = os.environ.get("TELEGRAM_NOTIFICATION_URL")
CHAT_ID = int(os.environ.get("CHAT_ID", 0))

if ACRA_TABLE_ID and TELEGRAM_NOTIFICATION_URL and CHAT_ID != 0:
    logger.info("Notification is set up.")

app = Sanic(__name__, log_config=logging_config)

session: Optional[ClientSession] = None

reports_table = Airtable(ACRA_BASE_ID, "reports", api_key=AIRTABLE_API_KEY)

REPORT_URL_TEMPLATE = f"https://airtable.com/{ACRA_TABLE_ID}/{{record_id}}"
NOTIFICATION_TEMPLATE = """**New ACRA report**:

**App:** {PACKAGE_NAME}
**Version:** {APP_VERSION_NAME}
**Phone:** {PHONE_MODEL}
**Android:** {ANDROID_VERSION}

{report_url}
"""


class AcraReport(BaseModel):
    ANDROID_VERSION: str = None
    APP_VERSION_CODE: int = None
    APP_VERSION_NAME: str = None
    APPLICATION_LOG: str = None
    AVAILABLE_MEM_SIZE: int = None
    BRAND: str = None
    BUILD: str = None
    BUILD_CONFIG: str = None
    CRASH_CONFIGURATION: str = None
    CUSTOM_DATA: str = None
    DEVICE_FEATURES: str = None
    DEVICE_ID: str = None
    DISPLAY: str = None
    DROPBOX: str = None
    DUMPSYS_MEMINFO: str = None
    ENVIRONMENT: str = None
    EVENTSLOG: str = None
    FILE_PATH: str = None
    INITIAL_CONFIGURATION: str = None
    INSTALLATION_ID: str = None
    IS_SILENT: bool = None
    LOGCAT: str = None
    MEDIA_CODEC_LIST: str = None
    PACKAGE_NAME: str = None
    PHONE_MODEL: str = None
    PRODUCT: str = None
    RADIOLOG: str = None
    REPORT_ID: str = None
    SETTINGS_GLOBAL: str = None
    SETTINGS_SECURE: str = None
    SETTINGS_SYSTEM: str = None
    SHARED_PREFERENCES: dict = None
    STACK_TRACE: str = None
    STACK_TRACE_HASH: str = None
    THREAD_DETAILS: str = None
    TOTAL_MEM_SIZE: int = None
    USER_APP_START_DATE: str = None
    USER_COMMENT: str = None
    USER_CRASH_DATE: str = None
    USER_EMAIL: str = None


async def send_notification(message: str, chat_id: int):
    if not (message or CHAT_ID or TELEGRAM_NOTIFICATION_URL):
        logger.info("Not sending notification. Is it configured?")
        return
    logger.info(
        "Sending notification through %r to %d", TELEGRAM_NOTIFICATION_URL, chat_id
    )
    global session
    if not session or session.closed:
        session = ClientSession(headers={"User-Agent": "telegram-bot/1.0"},)
    try:
        async with session.post(
            TELEGRAM_NOTIFICATION_URL,
            data=ujson.dumps(
                {"message": message, "chat_id": chat_id},
                ensure_ascii=False,
                escape_forward_slashes=False,
                indent=2,
            ),
        ) as r:
            if r.status == 204:
                logger.debug("message sent to %r: %r", chat_id, message)
                return True
            else:
                logger.debug("Failed to send message: %r", message)
                return False
    except ClientConnectorError as e:
        logger.error(e, exc_info=True)
        return None


@app.post("/acra")
async def acra_webhook(request: Request):
    logger.debug("message received: %r", request.body)
    try:
        event = AcraReport(**request.json)
        logger.debug("event: %r", event.dict())
        event_dict = event.dict()
        event_dict["SHARED_PREFERENCES"] = ujson.dumps(
            event_dict["SHARED_PREFERENCES"],
            ensure_ascii=False,
            escape_forward_slashes=False,
            indent=2,
        )
        record = reports_table.insert(event_dict)
        await send_notification(
            NOTIFICATION_TEMPLATE.format(
                **event_dict,
                report_url=REPORT_URL_TEMPLATE.format(record_id=record["id"]),
            ),
            CHAT_ID,
        )

    except ValidationError as e:
        logger.error(e, exc_info=True)
    except Exception as e:
        logger.error(e, exc_info=True)

    return response.empty()


if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=os.environ.get("PORT", 8000),
        debug=os.environ.get("DEBUG", False),
        access_log=True,
    )
