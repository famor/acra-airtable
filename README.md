# ACRA -> Airtable
This is a backend for [Application Crash Reports for Android (ACRA)](https://github.com/ACRA/acra) that uses [Airtable](https://airtable.com).

## How to use
Setup a base on Airtable with all the fields on `bot.py`
Run the script on a server and point ACRA in your Android app to send reports to: https://your.ip/acra.

Environment vars:

    AIRTABLE_API_KEY # get it from https://airtable.com/account
    ACRA_BASE_ID # get it from https://airtable.com/api
    
## Notifications
If set up, it can send telegram notifications using another app I created.
If you're also running that server, set up these vars to enable notifications:

- ACRA_TABLE_ID: get it from the URL when you open your ACRA table. It starts with "tbl"
- TELEGRAM_NOTIFICATION_URL: telegram-notifications server url
- CHAT_ID: telegram chat to send the message to

## Production

    python bot.py
    
or

    uvicorn bot:app --host=0.0.0.0 --port=8000 --log-level=info
  
## Dependencies
The main dependencies are:
- [airtable-python-wrapper](https://github.com/gtalarico/airtable-python-wrapper)
- [sanic: webserver](https://sanic.readthedocs.io/en/latest/)
- [uvicorn: ASGI server](https://www.uvicorn.org/)
- [pydantic: data validation](https://pydantic-docs.helpmanual.io/)
- [ujson: parsing json message](https://github.com/ultrajson/ultrajson) (regular json package fails)
- [aiohttp: download images and attachments](https://docs.aiohttp.org/en/stable/)

Install with either

    pip install -Ur requirements.txt
    
or 

    poetry install
